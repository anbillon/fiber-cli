// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"github.com/AlecAivazis/survey/v2/core"
	"github.com/spf13/cobra"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/storer"
)

var (
	fiberRsFileName = "fiber-rs.go"

	rsTemplate = `package main
{{ $dataLen := len .DataArr }}{{ if ne $dataLen 0 }}
import (
	"gitlab.com/anbillon/fiber"
)
{{- end }}

var _ = func() error {
{{- range $k, $v := .DataArr }}
	fiber.InitResourceFs("/fiber/{{ $v.Filename }}", 
		"{{ $v.Base64 }}")
{{- end }}

	return nil
}
`
)

type rsTemplateData struct {
	Filename string
	Base64   string
}

func newBuildCmd() *cobra.Command {
	return &cobra.Command{
		Use:                "build",
		Short:              "Build this project in production mode",
		DisableFlagParsing: true,
		RunE:               buildProject,
	}
}

func buildProject(_ *cobra.Command, args []string) error {
	if err := packResources(); err != nil {
		return err
	}

	defer func() {
		_ = os.Remove(fiberRsFileName)
	}()

	return build(args)
}

func packResources() error {
	dataArr := make([]rsTemplateData, 0)
	if err := filepath.Walk("./resources", func(path string,
		info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		content, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		dataArr = append(dataArr, rsTemplateData{
			Filename: path,
			Base64:   base64.StdEncoding.EncodeToString(content),
		})

		return nil
	}); err != nil {
		return err
	}

	content, err := core.RunTemplate(rsTemplate, map[string]interface{}{
		"DataArr": dataArr,
	})
	if err != nil {
		return err
	}

	return ioutil.WriteFile(fiberRsFileName, []byte(content), os.ModePerm)
}

func build(args []string) error {
	version, hash, err := retriveVersionAndHash()
	if err != nil {
		fmt.Printf("fail to retrive version and commit hash\n")
	}

	if len(version) == 0 {
		version = "dev"
	}
	if len(hash) == 0 {
		hash = "unkown"
	}

	buildArgs := []string{"build"}
	buildArgs = append(buildArgs, args...)
	buildArgs = append(buildArgs, "-ldflags")
	buildArgs = append(buildArgs, fmt.Sprintf("-w -s "+
		"-X 'gitlab.com/anbillon/fiber.Version=%s' "+
		"-X 'gitlab.com/anbillon/fiber.GitCommit=%s' "+
		"-X 'gitlab.com/anbillon/fiber.BuildTime=%s'",
		version, hash, time.Now().Format(time.RFC1123Z)))

	return runGoWithArgs(buildArgs)
}

func retriveVersionAndHash() (string, string, error) {
	repo, err := git.PlainOpen("./")
	if err != nil {
		return "", "", err
	}
	ref, err := repo.Head()
	if err != nil {
		return "", "", err
	}

	tagIter, err := repo.TagObjects()
	if err != nil {
		return "", "", err
	}

	var shortHash = ref.Hash().String()[:7]
	var latestVerTag = ""
	if err := tagIter.ForEach(func(tag *object.Tag) error {
		if tag.Hash == ref.Hash() {
			latestVerTag = tag.Name
			return storer.ErrStop
		}
		return nil
	}); err != nil {
		return "", "", err
	}

	return latestVerTag, shortHash, nil
}
