// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package generator

var readmeTemplate = `{{ .Name }}
=========
{{ .Description }}.

## Run
` + "`" + "`" + "`" + `
fiber serve
` + "`" + "`" + "`" + `
Visit http://localhost:3000/hello.

## Build
` + "`" + "`" + "`" + `
fiber build
` + "`" + "`" + "`" + `

For more details to visit [fiber][1]

[1]: https://gitlab.com/anbillon/fiber
`

type readmeProvider struct {
}

func (*readmeProvider) Filename() string {
	return "README.md"
}

func (*readmeProvider) Template() string {
	return readmeTemplate
}
