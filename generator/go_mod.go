// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).

package generator

var goModFileTemplate = `module {{ .ModuleName }}

go 1.13

require (
	gitlab.com/anbillon/fiber v0.0.4
{{- if eq .Engine "echo" }}
	github.com/labstack/echo/v4 v4.1.6
	gitlab.com/anbillon/fiber-echo v0.0.2
{{- else if eq .Engine "gin" }}
	github.com/gin-gonic/gin v1.4.0
	gitlab.com/anbillon/fiber-gin v0.0.1
{{- end }}
)
`

type goModProvider struct {
}

func (*goModProvider) Filename() string {
	return "go.mod"
}

func (*goModProvider) Template() string {
	return goModFileTemplate
}
