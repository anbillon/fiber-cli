// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package generator

import (
	"path/filepath"
)

var routerTemplate = `package routers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"
	"gitlab.com/anbillon/fiber"
	"gitlab.com/anbillon/fiber-echo"
)

type helloRouter struct {
}

func init() {
	fiber.Container().Wire(newHelloRouter)
}

func newHelloRouter(app *fiber.AppContext) engine.Router {
	log.Info().Msgf("appliction name: %v", app.GetString("fiber.application.name"))
	return &helloRouter{
	}
}

func (r *helloRouter) Register(e *echo.Echo) {
	e.GET("/hello", r.sayHello)
}

func (r *helloRouter) sayHello(c echo.Context) error {
	return c.String(http.StatusOK, "hello")
}
`

type routerProvider struct {
}

func (*routerProvider) Filename() string {
	return filepath.Join("routers", "hello_router.go")
}

func (*routerProvider) Template() string {
	return routerTemplate
}
