// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package generator

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/AlecAivazis/survey/v2/core"
)

// ProjectInfo defines the information of the project to create.
type ProjectInfo struct {
	Name        string
	ModuleName  string
	Description string
	Engine      string
}

type provider interface {
	// filename to write the content
	Filename() string

	// template of this provider
	Template() string
}

type generator struct {
	providers []provider
}

func NewGenerator() *generator {
	gf := &generator{}

	gf.providers = append(gf.providers, &goModProvider{}, &gitIgnoreProvider{},
		&readmeProvider{}, &projectMainProvider{}, &routerProvider{},
		&applicationProvider{})

	return gf
}

// Make will create the project folder and generate all the template files.
func (gf *generator) Make(info *ProjectInfo) error {
	if err := os.MkdirAll(info.Name, os.ModePerm); err != nil {
		return err
	}

	for _, g := range gf.providers {
		data, err := core.RunTemplate(g.Template(), info)
		if err != nil {
			return err
		}

		filePath := filepath.Join(info.Name, g.Filename())
		dir := filepath.Dir(filePath)
		if dir != info.Name {
			if err := os.Mkdir(dir, os.ModePerm); err != nil {
				return err
			}
		}
		if err := ioutil.WriteFile(filePath, []byte(data), os.ModePerm); err != nil {
			return err
		}
	}

	return nil
}
