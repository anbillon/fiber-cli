module gitlab.com/anbillon/fiber-cli

go 1.13

require (
	github.com/AlecAivazis/survey/v2 v2.0.1
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b
	github.com/spf13/cobra v0.0.5
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	gopkg.in/src-d/go-git.v4 v4.12.0
)
