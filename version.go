// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"runtime"

	"github.com/spf13/cobra"
)

var (
	Version   = "0.1.0-dev"
	GitCommit = "unkown"
	BuildTime = "unkown"
)

func newVersionCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Show the version information",
		Run:   showVersion,
	}
}

func showVersion(_ *cobra.Command, _ []string) {
	fmt.Printf("Version:    %v\n"+
		"Go version: %v\n"+
		"Git commit: %v\n"+
		"Built:      %v\n",
		Version, runtime.Version(), GitCommit, BuildTime)
}
