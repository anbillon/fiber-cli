#!/bin/bash

function genVersion() {
    latestVersion="dev"

    tag=$(git describe --tags 2> /dev/null)
    if [[ $? -ne 0 ]]; then
        echo ${latestVersion}
    fi

    if [[ ${tag} =~ (.*)-([0-9]+)-g([0-9,a-e]{7}) ]]; then
        echo ${latestVersion}
    else
        echo "${tag}"
    fi
}

echo $(genVersion)
