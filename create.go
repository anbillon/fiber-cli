// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/AlecAivazis/survey/v2"
	"github.com/AlecAivazis/survey/v2/terminal"
	"github.com/mgutz/ansi"
	"github.com/spf13/cobra"
	"gitlab.com/anbillon/fiber-cli/generator"
	"gopkg.in/src-d/go-git.v4"
)

func newCreateCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "create <app-name>",
		Short: "Create a new fiber project",
		Run:   createWithoutError,
	}
}

func createWithoutError(cmd *cobra.Command, args []string) {
	if len(args) != 1 {
		_ = cmd.Help()
		fmt.Printf("\n%v\n", ansi.Color("Missing required argument <app-name>.", "red"))
		return
	}

	if err := createProject(cmd, args); err != nil {
		if err == terminal.InterruptErr {
			return
		} else {
			fmt.Println(cmd.UsageString())
			fmt.Printf("%v\n", ansi.Color(err.Error(), "red"))
		}
	}
}

func createProject(_ *cobra.Command, args []string) error {
	fmt.Println(ansi.Color(fmt.Sprintf("\U0001F320 Fiber CLI v%s", Version), "blue+h"))

	var projectName = args[0]
	projectPath, err := filepath.Abs(projectName)
	if err != nil {
		return err
	}

	if result, err := exists(projectName); err != nil {
		return err
	} else if result {
		var action string
		if err := survey.AskOne(&survey.Select{
			Message: fmt.Sprintf("Project directory %s already exists. "+
				"Pick an action:", ansi.Color(projectPath, "yellow")),
			Options: []string{"Override", "Cancel"},
		}, &action); err != nil {
			return err
		}

		if action == "Cancel" {
			return nil
		} else {
			// override will removing the existed directory
			cleanUpTip := fmt.Sprintf("\n\U0001F5D1  Removing directory %v...\n",
				ansi.Color(projectPath, "yellow+h"))
			fmt.Println(cleanUpTip)
			if err := os.RemoveAll(projectPath); err != nil {
				return err
			}
		}
	}

	var projectInfo = new(generator.ProjectInfo)
	if err := survey.Ask(makeQuestions(), projectInfo); err != nil {
		return err
	}
	projectInfo.Name = projectName

	generatorTip := fmt.Sprintf("\n\U00002728 Creating project in %v...",
		ansi.Color(projectPath, "green"))
	fmt.Println(generatorTip)
	if err := generator.NewGenerator().Make(projectInfo); err != nil {
		return err
	}

	if _, err := git.PlainInit(filepath.Join(projectPath, ".git"), true); err != nil {
		return err
	}

	successTip := fmt.Sprintf("\n\U0001F389 Successfully created project %v.",
		ansi.Color(projectInfo.Name, "yellow"))
	fmt.Printf("%v\n\n", successTip)

	return nil
}

func makeQuestions() []*survey.Question {
	return []*survey.Question{
		{
			Name: "ModuleName",
			Prompt: &survey.Input{
				Message: "Please input module name, eg: github.com/anbillon/fiber",
				Default: "example.com/demo",
			},
		},
		{
			Name: "Description",
			Prompt: &survey.Input{
				Message: "Please input description of this project",
				Default: "Demo project for fiber",
			},
		},
		{
			Name: "Engine",
			Prompt: &survey.Select{
				Message: "Please pick a server engine:",
				Options: []string{"echo", "gin"},
			},
		},
	}
}
