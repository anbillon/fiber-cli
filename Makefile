VERSION=$(shell ./gen_version.sh)
GIT_COMMIT=$(shell git rev-parse --short HEAD)
BUILD_TIME=$(shell date -R)
BIN_NAME=fiber

.PHONY: build
build:
	CGO_ENABLED=0 go build -a -tags netgo -ldflags \
        "-w -s \
         -X 'main.Version=${VERSION}' \
         -X 'main.GitCommit=${GIT_COMMIT}' \
         -X 'main.BuildTime=${BUILD_TIME}'" -o ${BIN_NAME}

.PHONY: install
install:
	mv ${BIN_NAME} ${realpath ${GOPATH}/bin/}

.PHONY: clean
clean:
	rm -rf $(BIN_NAME)